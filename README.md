# JSON
JSON stands for JavaScript Object Notation. JSON objects are used for transferring data between server and client, XML serves the same purpose. However JSON objects have several advantages over XML and we are going to discuss them in this tutorial along with JSON concepts and its usages.

Let's have a look at the piece of a JSON data. It basically has key-value pairs.
```JSON
	var user = {
	   "firstName" : "Koyel",
	   "lastName" : "Ghosh",
	   "phone_number" : 9830123456,
	   "is_female" : true
	};
```
where, `user` is a variable. `firstName`, `lastName`, `phone_number`, and `is_female` are the keys and `Koyel`, `Ghosh`, `9830123456`, and `true` are the values respectively.
---
## Basic Constructs
There are four basic and built-in data types in JSON. They are *strings, numbers, booleans (i.e true and false) and null*. 

Besides, there are two data types which are structured - objects and arrays.
Objects are wrapped within `'{' and '}'`. Arrays are enclosed by `'[' and ']'`. Objects are a list of label-value pairs. Arrays are list of values.
Both objects and arrays can be nested.
strings, numbers, booleans (i.e true and false) and null can be used as values.

*The following image and then text following will be useful to get you started with how JSON data is constructed.*

![json_structure](https://cd.rf.gd/cdn/json_structure.png)

So the entire content of the JSON data shown above is enclosed within an object. `"Title": "The Cuckoo's Calling", "Author": "Robert Galbraith", "Genre": "classic crime novel"`, these are label-value pairs separated by commas. Labels and their values are separated by a colon (:). Notice that both labels and values are enclosed by quotations, since they are strings.
Notice the `'"Detail"'` label then. It contains another object, which again contains several label-value pairs. This is an example of how nesting (object within object in this case) is done in JSON.
Then `'"Price"'` label contains an array, which is turn contains two separate objects. Another example of nesting.

Also, notice that numbers are not enclosed by quotations.

---
## Features of JSON:

1. It is light-weight
2. It is language independent
3. Easy to read and write
4. Text based, human readable data exchange format

---
## JSON vs. XML
Let see how JSON and XML look when we store the records of 2 users in a text based format so that we can retrieve it later when required.
As you can clearly see JSON is much more light-weight compared to XML. Also, in JSON we take advantage of arrays that is not available in XML.

* JSON:
```JSON
  var users = [{
   "first_name" : "Koyel",
   "last_name" : "Ghosh",
   "phone_number" : 9830123456,
   "is_female" : true
  },
  {
   "first_name" : "Piuli",
   "last_name" : "Saha",
   "phone_number" : null,
   "is_female" : true
  }
  ];
```
  
* XML:
```XML
<?xml version="1.0" encoding="UTF-8"?>
<users>
   <user>
      <first_name>Koyel</first_name>
      <last_name>Ghosh</last_name>
      <phone_number>9830123456</phone_number>
      <is_female>true</is_female>
   </user>
   <user>
      <first_name>Piuli</first_name>
      <last_name>Saha</last_name>
      <phone_number />
      <is_female>true</is_female>
   </user>
</users> 
```
---
## JSON data structure types

**1) JSON objects:**
```JSON
      var user = {
      "first_name" : "Koyel",
      "last_name" : "Ghosh",
      "phone_number" : 9830123456,
      "is_female" : true
    };
```

The above text creates an object that we can access using the variable user. Inside an object we can have any number of key-value pairs like we have above. 

We can access the information out of a JSON object like this:
```CSS
user.name --> Koyel
user.last_name --> Ghosh
user.phone_number --> 9830123456
user.is_female --> true
```

---

**2) JSON objects in array:**

In the above example we have stored the information of one person in a JSON object suppose we want to store the information of more than one person; in that case we can have an array of objects.
Better readability user object can be renamed as users.

```JSON
var users = [{
   "first_name" : "Koyel",
   "last_name" : "Ghosh",
   "phone_number" : 9830123456,
   "is_female" : true
},
{
   "first_name" : "Piuli",
   "last_name" : "Saha",
   "phone_number" : null,
   "is_female" : true
}
];
```

We can access the information out of a JSON object like this:
```CSS
users[0].first_name --> Koyel
users[0].phone_number --> 9830123456
users[1].first_name --> Piuli
users[1].is_female --> true
```
---

**3) Nesting JSON objects:**
Another way of doing the same thing that we have done above.

```JSON

var users = {
  "koyel" : {
  "phone_number" : 9830123456,
   "is_female" : true,
	"city" : "Kolkata"   
},
"piuli" : {
  "phone_number" : null,
   "is_female" : true,
   "city" : "Mumbai"
},
"dodo" : {
  "phone_number" : 4985212315,
   "is_female" : false,
   "city" : "Delhi"
};
```

---